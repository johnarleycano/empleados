<?php
date_default_timezone_set('America/Bogota');

defined('BASEPATH') OR exit('El acceso directo a este archivo no está permitido');

class Inicio extends CI_Controller {
    function index() {
        $this->data['contenido_principal'] = 'inicio/index';
        $this->load->view('core/template', $this->data);
    }
}