<div class="sidebar sidebar-dark sidebar-fixed" id="sidebar">
<div class="sidebar-brand d-none d-md-flex">
<svg class="sidebar-brand-full" width="118" height="46" alt="CoreUI Logo">
<use xlink:href="assets/brand/coreui.svg#full"></use>
</svg>
<svg class="sidebar-brand-narrow" width="46" height="46" alt="CoreUI Logo">
<use xlink:href="assets/brand/coreui.svg#signet"></use>
</svg>
</div>
<ul class="sidebar-nav" data-coreui="navigation" data-simplebar="">
<li class="nav-item"><a class="nav-link" href="index.html">
<svg class="nav-icon">
<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-speedometer"></use>
</svg> Inicio<span class="badge badge-sm bg-info ms-auto">NEW</span></a></li>
<li class="nav-title">Tema</li>
<li class="nav-item"><a class="nav-link" href="colors.html">
<svg class="nav-icon">
<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-drop"></use>
</svg> Colores</a></li>
<li class="nav-item"><a class="nav-link" href="typography.html">
<svg class="nav-icon">
<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
</svg> Tipografia</a></li>
<li class="nav-title">Componentes</li>
<li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
<svg class="nav-icon">
<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-puzzle"></use>
</svg> Base</a>
<ul class="nav-group-items">
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>base/accordion.html"><span class="nav-icon"></span> Acordeón</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>base/breadcrumb.html"><span class="nav-icon"></span> Migaja de pan</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>base/cards.html"><span class="nav-icon"></span> Tarjetas</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>base/carousel.html"><span class="nav-icon"></span> Carrusel</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>base/collapse.html"><span class="nav-icon"></span> Collapse</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>base/list-group.html"><span class="nav-icon"></span> Grupo de Lista</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>base/navs-tabs.html"><span class="nav-icon"></span> Navegadores &amp; Pestañas</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>base/pagination.html"><span class="nav-icon"></span> Paginación</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>base/placeholders.html"><span class="nav-icon"></span> Marcadores de Posición</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>base/popovers.html"><span class="nav-icon"></span> Popovers</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>base/progress.html"><span class="nav-icon"></span> Progreso</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>base/scrollspy.html"><span class="nav-icon"></span> Desplazamiento</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>base/spinners.html"><span class="nav-icon"></span> Spinners</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>base/tables.html"><span class="nav-icon"></span> Tablas</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>base/tooltips.html"><span class="nav-icon"></span> Información sobre Herramientas</a></li>
</ul>
</li>
<li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
<svg class="nav-icon">
<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-cursor"></use>
</svg> Botones</a>
<ul class="nav-group-items">
<li class="nav-item"><a class="nav-link" href="buttons/buttons.html"><span class="nav-icon"></span> Botones</a></li>
<li class="nav-item"><a class="nav-link" href="buttons/button-group.html"><span class="nav-icon"></span> Grupo de Botones</a></li>
<li class="nav-item"><a class="nav-link" href="buttons/dropdowns.html"><span class="nav-icon"></span> Listas Desplegables</a></li>
</ul>
</li>
<li class="nav-item"><a class="nav-link" href="charts.html">
<svg class="nav-icon">
<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-chart-pie"></use>
</svg>Gráficos</a></li>
<li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
<svg class="nav-icon">
<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-notes"></use>
</svg> Formularios</a>
<ul class="nav-group-items">
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>forms/form-control.html"> Control de Formulario</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>forms/select.html">Seleccionar</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>forms/checks-radios.html"> Cheques y Radios</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>forms/range.html"> Rango</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>forms/input-group.html"> Grupo de entrada</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>forms/floating-labels.html">Etiquetas Flotantes</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>forms/layout.html"> Disposición</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>forms/validation.html"> Validación</a></li>
</ul>
</li>
<li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
<svg class="nav-icon">
<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-star"></use>
</svg> Iconos</a>
<ul class="nav-group-items">
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>icons/coreui-icons-free.html"> Iconos interfaz de usuario<span class="badge badge-sm bg-success ms-auto">Free</span></a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>icons/coreui-icons-brand.html"> CoreUI Iconos- Marca</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>icons/coreui-icons-flag.html"> CoreUI Iconos-Bandera</a></li>
</ul>
</li>
<li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
<svg class="nav-icon">
<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-bell"></use>
</svg> Notificaciones</a>
<ul class="nav-group-items">
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>notifications/alerts.html"><span class="nav-icon"></span> Alertas</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>notifications/badge.html"><span class="nav-icon"></span>Insignia</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>notifications/modals.html"><span class="nav-icon"></span> Modales</a></li>
<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>notifications/toasts.html"><span class="nav-icon"></span> Tostadas</a></li>
</ul>
</li>
<li class="nav-item"><a class="nav-link" href="widgets.html">
<svg class="nav-icon">
<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-calculator"></use>
</svg> Widgets<span class="badge badge-sm bg-info ms-auto">NEW</span></a></li>
<li class="nav-divider"></li>
<li class="nav-title">Extras</li>
<li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
<svg class="nav-icon">
<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-star"></use>
</svg> Páginas</a>
<ul class="nav-group-items">
<li class="nav-item"><a class="nav-link" href="login.html" target="_top">
<svg class="nav-icon">
<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-account-logout"></use>
</svg> Acceso</a></li>
<li class="nav-item"><a class="nav-link" href="register.html" target="_top">
<svg class="nav-icon">
<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-account-logout"></use>
</svg>Registro</a></li>
<li class="nav-item"><a class="nav-link" href="404.html" target="_top">
<svg class="nav-icon">
<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-bug"></use>
</svg> Error 404</a></li>
<li class="nav-item"><a class="nav-link" href="500.html" target="_top">
<svg class="nav-icon">
<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-bug"></use>
</svg> Error 500</a></li>
</ul>
</li>
<li class="nav-item mt-auto"><a class="nav-link" href="https://coreui.io/docs/templates/installation/" target="_blank">
<svg class="nav-icon">
<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-description"></use>
</svg> Documentos</a></li>
<li class="nav-item"><a class="nav-link nav-link-danger" href="https://coreui.io/pro/" target="_top">
<svg class="nav-icon">
<use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-layers"></use>
</svg> Prueba CoreUI
<div class="fw-semibold">PRO</div>
</a></li>
</ul>
<button class="sidebar-toggler" type="button" data-coreui-toggle="unfoldable"></button>
</div>