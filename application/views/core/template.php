<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v4.2.2
* @link https://coreui.io
* Copyright (c) 2022 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->
<!-- Breadcrumb-->
<html lang="es">
    <head>
        <?php $this->load->view('core/header'); ?>
    </head>
    <body>
        <?php $this->load->view('core/menu_lateral'); ?>
        <?php $this->load->view('core/menu_superior'); ?>
        <?php $this->load->view($contenido_principal); ?>
        <?php $this->load->view('core/footer'); ?>
    </body>
</html>